import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.awt.Robot;
import java.io.FileNotFoundException;

public class QnAReader
{

	private ArrayList<ArrayList<String>> qNa;

	public QnAReader() {
		try 
		{
			readQuestions();
		} 
		catch(FileNotFoundException e) 
		{
			System.out.println(e.getMessage());
			System.exit(0);
		}
	}

	public void printQuestions() 
	{
		for(int i = 0; i < qNa.size(); i++) {
			System.out.println("Question " + i + ":");
			for(int a = 0; a < qNa.get(i).size(); a++) {
				System.out.println(qNa.get(i).get(a));
			}
			System.out.println("-\n-\n-");
		}
	}

	public ArrayList<ArrayList<String>> getQuestionsAnswers() {
		return qNa;
	}

	private void readQuestions() throws FileNotFoundException 
	{
		File questions;
		Scanner stdin;
		int questionNumber;
	    	String nextLine;	

		qNa = new ArrayList<ArrayList<String>>();

		questions = new File("questions.txt");
		stdin = new Scanner(questions);

		questionNumber = 0;

		do  {
			qNa.add(new ArrayList<String>());

			while(stdin.hasNextLine())
			{
				nextLine = stdin.nextLine();
				if (nextLine.equals(""))
					break;
				else
					qNa.get(questionNumber).add(nextLine);
			}
			questionNumber++;

		} while(stdin.hasNextLine());
	}
}

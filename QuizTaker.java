import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.GridLayout;

public class QuizTaker implements Runnable {

	private ArrayList<ArrayList<String>> qNa;

	public QuizTaker() {
		QnAReader reader = new QnAReader();
		qNa = reader.getQuestionsAnswers();

		run();
	}

	@Override
	public void run() {
		JFrame f = new JFrame("Quiz Taker");
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.setLayout(new GridLayout(0,1));

		for(int i = 0; i < qNa.size(); i++) { 
			JButton b = new JButton();
			b.setText(qNa.get(i).get(0));
			b.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("button pressed");
				}
			});
			f.add(b);
		}

		f.pack();
		f.setVisible(true);
	}
	public static void main(String[] args) {
		QuizTaker qt = new QuizTaker();
	}
}

